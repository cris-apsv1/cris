package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Researcher;


@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher r = (Researcher) request.getSession().getAttribute("user");
		if ( r!=null && r.getId().equals("root")) {
			getServletContext().getRequestDispatcher("/AdminView.jsp").forward(request, response);
		    String id = request.getParameter("id");
		    String name = request.getParameter("name");
		    String email = request.getParameter("email");
		    String lastname = request.getParameter("lastname");
	        Client client = ClientBuilder.newClient(new ClientConfig());
			Researcher researcher = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/" 
			         + id)
			        .request().accept(MediaType.APPLICATION_JSON).get(Researcher.class);  
			if(researcher !=null) {
				Researcher n = new Researcher();
				n.setId(id);
				n.setName(name);
				n.setLastname(lastname);
				n.setEmail(email);
				client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/" 
				         + id).request().post(Entity.entity(n, MediaType.APPLICATION_JSON), Response.class);
		        response.sendRedirect(request.getContextPath() + "/AdminServlet");
			}
		}
		else {
			request.setAttribute("message", "Invalid user or password");
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		}
		
		}
}
