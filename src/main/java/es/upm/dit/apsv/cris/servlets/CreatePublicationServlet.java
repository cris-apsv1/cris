package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;


@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Publication r = (Publication) request.getSession().getAttribute("user");
		if ( r!=null && r.getId().equals("root")) {
			getServletContext().getRequestDispatcher("/AdminView.jsp").forward(request, response);
		    String id = request.getParameter("id");
		    String title = request.getParameter("title");
		    String publicationName = request.getParameter("publicationName");
		    String publicationDate = request.getParameter("publicationDate");
		    String authors = request.getParameter("authors");
		    String citeCount = request.getParameter("citeCount");

	        Client client = ClientBuilder.newClient(new ClientConfig());
			Publication Publication = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/" 
			         + id)
			        .request().accept(MediaType.APPLICATION_JSON).get(Publication.class);  
			if(Publication !=null) {
				Publication n = new Publication();
				n.setId(id);
				n.setTitle(title);
				n.setAuthors(authors);
				n.setPublicationDate(publicationDate);
				n.setCiteCount(Integer.valueOf(citeCount));
				client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/" 
				         + id).request().post(Entity.entity(n, MediaType.APPLICATION_JSON), Response.class);
		        response.sendRedirect(request.getContextPath() + "/AdminServlet");
			}
		}
		else {
			request.setAttribute("message", "Invalid user or password");
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		}
		
		}
}
